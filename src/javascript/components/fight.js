import { controls } from '../../constants/controls';

class Fighter {
  constructor(fighter, position, player, opponent) {
    const {attack, defense, health: _health} = fighter;
    Object.assign(this, {attack, defense, _health})

    this.player = player;
    this.healthBar = document.getElementById(`${position}-fighter-indicator`);
    this.origin = fighter;
    this.opponent = opponent;
    this.lastComboTime = new Date(0);
  }

  set health(hp) {
   this._health = hp;

   let healthInPercents = 100 * this._health / this.origin.health;
   healthInPercents = (healthInPercents >= 0) ? healthInPercents : 0;

   this.healthBar.style.width =  String(healthInPercents) + "%";
  }

  get health() {
    return this._health;
  }
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let fighterOne = new Fighter(firstFighter, "left", "PlayerOne"),
      fighterTwo = new Fighter(secondFighter, "right", "PlayerTwo", fighterOne);
    fighterOne.opponent = fighterTwo;

    addKeyMap(fighterOne, resolve);
    addKeyMap(fighterTwo, resolve);
  });
}

export function addKeyMap(fighter, resolve) {
  let keys = new Set;

  document.addEventListener('keydown', e => {
    if (e.code === controls[fighter.player + "Block"] && fighter.status == null) {
      fighter.status = "defending";
    } else if (e.code === controls[fighter.player + "Attack"] && fighter.status == null) {
      fighter.status = "attacking";
      if (fighter.opponent.status !== 'defending') {
        harmEnemy(getDamage(fighter, fighter.opponent), fighter.opponent);
      }
    } else if (isInCombo(e.code)) {
        keys.add(e.code);

        if (keys.size === 3) {
          fighter.status = "combo";
          harmEnemy(parseInt(tryCombo(fighter)), fighter.opponent);
        }
    }

    if (fighter.opponent.health <= 0) {
      resolve(fighter.origin);
    }
  })

  document.addEventListener('keyup', e => {
    if (e.code === controls[fighter.player + "Attack"] && fighter.status === "attacking") {
      fighter.status = null;
    } else if (e.code === controls[fighter.player + "Block"] && fighter.status === "defending") {
      fighter.status = null;
    } else if (isInCombo(e.code) && fighter.status === "combo") {
      keys.delete(e.code);
      fighter.status = null;
    }
  })

  function isInCombo(key) {
    const comboSet = controls[fighter.player + "CriticalHitCombination"];
    return comboSet.indexOf(key) !== -1;
  }
}

export function tryCombo(fighter) {
  const now = Date.now();
  const timeToWait = now - fighter.lastComboTime;

  if (10000 <= timeToWait) {
    fighter.lastComboTime = new Date;
    return fighter.attack * 2;
  } else {
    console.warn(`Not yet. Please wait ${10 - timeToWait/1000}s`)
    return 0;
  }
}

export function harmEnemy(damage, enemy) {
  enemy.health -= damage;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage > 0) ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random()+1;
  console.log(fighter.attack * criticalHitChance);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random()+1;
  console.log(fighter.defense * dodgeChance);
  return fighter.defense * dodgeChance;
}
