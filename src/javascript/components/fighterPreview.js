import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!!fighter) {
    const wrapper = createElement({tagName: 'div', className: 'fighter-preview___wrapper'});

    const name = createElement({
      tagName: 'h3',
      className: 'fighter-preview___title',
    })

    name.innerHTML = fighter.name
    wrapper.append(name);

    const chars = ["health", "attack", "defense"];

    for (let prop of chars) {
      const element = createElement({
        tagName: 'div',
        className: `fighter-preview___${prop}`,
      });

      element.innerHTML = prop[0].toUpperCase() + prop.slice(1) + ": " + fighter[prop];

      wrapper.append(element);
    }

    fighterElement.append(wrapper);
    fighterElement.append(createFighterImage(fighter));
  }
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
