import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter) {
  const root = document.getElementById('root');

  console.log(fighter);
  showModal({
    title: "Game over",
    bodyElement: `${fighter.name} wins`,
    onClose() {
      root.innerHTML = '';
      new App();
    }
  });
}
