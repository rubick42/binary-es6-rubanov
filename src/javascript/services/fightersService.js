import { callApi } from '../helpers/apiHelper';
import { createFighterPreview } from '../components/fighterPreview';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
  }
  // TODo Delete this
  // async getfighterinfo(id) {
  //   const fighter = await this.getfighterdetails(id);
  //   createfighterpreview(fighter, 'left');
  // }
}

export const fighterService = new FighterService();
